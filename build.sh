#!/bin/sh
gcc -O2 mtrack.c -shared -o mtrack.so -ldl -pthread -fPIC -g
gcc -O2 mtdump.c -o mtdump -g `pkg-config --cflags --libs eina`
gcc -O2 mtshow.c -o mtshow -g `pkg-config --cflags --libs eina`

sudo rm -f /usr/local/bin/mtshow
sudo rm -f /usr/local/bin/mtdump
sudo rm -f /usr/local/lib/mtrack.so
sudo rm -f /usr/local/bin/mtf
sudo rm -f /usr/local/bin/mtt
sudo cp mtshow /usr/local/bin/mtshow
sudo cp mtdump /usr/local/bin/mtdump
sudo cp mtrack.so /usr/local/lib/mtrack.so
sudo cp mtf /usr/local/bin/mtf
sudo cp mtt /usr/local/bin/mtt
sudo cp mtd /usr/local/bin/mtd

#rm -f mtrack.so
#rm -f mtdump
rm -f *~
